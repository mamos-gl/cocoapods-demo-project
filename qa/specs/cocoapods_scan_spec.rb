require 'json'
require 'rspec'

describe "when scanning mamos-gl/cocoapods-demo-project" do
  let(:expectations_dir) { "qa/expect" }
  let(:sbom_path) { File.join(expectations_dir, "gl-sbom-cocoapods-cocoapods.cdx.json") }
  let(:report_path) { File.join(expectations_dir, "gl-dependency-scanning-report.json") }

  describe "CycloneDX SBOM" do
    it "should exist and be a valid JSON file" do
      expect(File.exist?(sbom_path)).to be true
      expect { JSON.parse(File.read(sbom_path)) }.not_to raise_error
    end

    it "should contain expected keys" do
      sbom_content = JSON.parse(File.read(sbom_path))
      expect(sbom_content).to include('bomFormat', 'specVersion', 'version', 'components')
    end
  end

  describe "Dependency Scanning Report" do
    let(:report_content) { JSON.parse(File.read(report_path)) }
    let(:vulnerabilities) { report_content['vulnerabilities'] }

    it "should exist and be a valid JSON file" do
      expect(File.exist?(report_path)).to be true
      expect { JSON.parse(File.read(report_path)) }.not_to raise_error
    end

    it "should contain vulnerabilities array" do
      expect(vulnerabilities).to be_an(Array)
    end

    context "when examining vulnerabilities" do
      it "should be valid whether empty or not" do
        if vulnerabilities.empty?
          expect(vulnerabilities).to be_empty
          puts "Vulnerabilities array is empty, which is acceptable."
        else
          expect(vulnerabilities).not_to be_empty
          vulnerabilities.each do |vulnerability|
            expect(vulnerability).to include(
              'id',
              'name',
              'description',
              'severity',
              'solution',
              'location',
              'identifiers',
              'cvss_vectors',
              'links',
              'details'
            )

            expect(vulnerability['severity']).to be_a(String)
            expect(vulnerability['solution']).to be_a(String)

            expect(vulnerability['location']).to include(
              'file',
              'dependency'
            )
            expect(vulnerability['location']['dependency']).to include(
              'package',
              'version'
            )

            expect(vulnerability['identifiers']).to be_an(Array)
            vulnerability['identifiers'].each do |identifier|
              expect(identifier).to include('type', 'name', 'value', 'url')
            end

            expect(vulnerability['cvss_vectors']).to be_an(Array)
            vulnerability['cvss_vectors'].each do |vector|
              expect(vector).to include('vendor', 'vector')
            end

            expect(vulnerability['links']).to be_an(Array)
            vulnerability['links'].each do |link|
              expect(link).to include('url')
            end

            expect(vulnerability['details']).to include('vulnerable_package')
            expect(vulnerability['details']['vulnerable_package']).to include(
              'type',
              'name',
              'value'
            )
          end
        end
      end
    end
  end

  describe "Project structure" do
    it "should have a Podfile.lock file" do
      expect(File.exist?("Podfile.lock")).to be true
    end

    it "should have Cocoapods source files" do
      swift_files = Dir.glob("**/Podfile")
      expect(swift_files).not_to be_empty
    end
  end
end
