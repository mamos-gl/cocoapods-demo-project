# swift-cocoapods

Test project with:

* **Language:** Swift
* **Package Manager:** Cocoapods
* **Framework:** IOS

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use-a-test-project) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported                 |
|---------------------|---------------------------|
| SAST                |  :x: |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  |  :x: |
| DAST                |  :x: |
| License Management  |  :x: |
